package com.example.Assignment2.Controller;

import com.example.Assignment2.Entity.Employee;
import com.example.Assignment2.Service.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
public class MyController {
	@Autowired
	EmployeeRepository employeeRepository;
	@GetMapping(value="/home")
	public String home(){
		return "api created successfully";
	}

	@GetMapping(value = "/employee")
	public List getEmployee(){
		return (List) employeeRepository.findAll();

	}
	@PostMapping(value ="/employee")
	public Employee createEmployee(@RequestBody Employee employee){
		return (Employee) employeeRepository.save(employee);

	}
	@GetMapping(value="/employee/{id}")
	public Employee getEmployeeById(@PathVariable("id")int id){
		return employeeRepository.getById(id);
	}
	@DeleteMapping(value="/employee/{id}")
	public String deleteEmployeeById(@PathVariable("id") int id){
		employeeRepository.deleteById(id);
		return "Employee data deleted successfully";
	}
	@PutMapping( value = "/employee/{id}")
	public Employee updateEmployee(@RequestBody Employee employee ,@PathVariable("id") int id){
		Employee emp=employeeRepository.getById(id);
		emp.setId(employee.getId());
		emp.setName(employee.getName());
		emp.setCity(employee.getCity());
		emp.setDesignation(employee.getDesignation());
		return emp;
	}


}



