package com.example.Assignment2.Entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import org.hibernate.proxy.HibernateProxy;



@Entity
@Table(name="employee")
public class Employee {
	@Id

	private int id;
	private String name;
	private String city;
	private String designation;

	@Override
	public String toString() {
		return "Employee{" +
				"id=" + id +
				", name='" + name + '\'' +
				", city='" + city + '\'' +
				", designation='" + designation + '\'' +
				'}';
	}

	public Employee() {
	}
	@JsonIgnore
	private HibernateProxy hibernateLazyInitializer;


	public Employee(int id, String name, String city, String designation) {
		this.id = id;
		this.name = name;
		this.city = city;
		this.designation = designation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}
}


