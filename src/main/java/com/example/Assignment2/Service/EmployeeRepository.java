package com.example.Assignment2.Service;

import com.example.Assignment2.Entity.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface EmployeeRepository extends CrudRepository <Employee,Integer>{
	//Employee updateById(Employee employee, int id);

	Employee getById(int id);
}
